<?php
/**
 *
 * @wordpress-plugin
 * Plugin Name:       Page Info
 * Plugin URI:        http://bhwd.me/page-info
 * Description:       Page Info allows you to view the following
 * Version:           0.0.1
 * Author:            Ben Hussenet
 * Author URI:        http://bhwd.me
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       page-info
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}





function page_info_enqueue() {
   
    wp_enqueue_style( 'pageinfo_css', plugin_dir_url( __FILE__ ) . 'css/page-info.css' );
    wp_enqueue_style( 'bootstrap', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css' );
    wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' );
    
	
    wp_enqueue_script( 'pageinfo_js', plugin_dir_url( __FILE__ ) . 'js/page-info.js', array(), '1.0.0', true );
    
    //wp_enqueue_script( 'zclip_js', plugin_dir_url( __FILE__ ) . 'js/jquery.zeroclipboard.min.js');
}
add_action( 'wp_enqueue_scripts', 'page_info_enqueue' );



function page_info_panel()
{

global $user_ID; if( $user_ID ) :
if( current_user_can('update_core') ) :

//global $post;

$parentTitle = array_pop(get_post_ancestors($post->ID))->post_title;
$parentID = array_pop(get_post_ancestors($post->ID));
$currentTitle = get_the_title($post);
$currentID = get_the_ID();
$bodyclasses = get_body_class($post);
$parentTitleTwo = get_the_title($post->post_parent);

$current = $post->ID;
$parent = $post->post_parent;
$grandparent_get = get_post($parent);
$grandparent = $grandparent_get->post_parent;



 echo "<div class='settings panel panel-default hidden-xs hidden-sm hidden-print ng-scope' id='PageInfo-settings' data-collapse='accordion persist'><!-- start: SETTINGS -->
<button class='btn btn-default ng-scope'>
    <i class='fa fa-spin fa-gear'></i>
</button>
<div class='panel-heading ng-scope'>Current Page Info <i class='fa fa-caret-square-o-up pull-right'></i></div>
<div class='panel-body ng-scope'>
    <!-- start: FIXED HEADER -->";

    

  
 if ( is_page()) { 

   if ($root_parent = get_the_title($grandparent) !== $root_parent = get_the_title($current)) {
   
   echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Parent Page Title:</span>
        <span class='setting-title pull-right'> ".get_the_title($grandparent)."</span>
    </div>";
   
   echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Parent Page ID:</span>
        <span class='setting-title pull-right'> " . $grandparent . "</span>
    </div>";
   
   echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Current Page Title:</span>
        <span class='setting-title pull-right'> " . $currentTitle . "</span>
    </div>";

echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Current Page ID:</span>
        <span class='setting-title pull-right'> " . $currentID . "</span>
    </div>"; 

    }else {
    
      echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Current Page Title:</span>
        <span class='setting-title pull-right'> " . get_the_title($parent) . "</span>
    </div>";

echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Current Page ID:</span>
        <span class='setting-title pull-right'> " . $currentID . "</span>
    </div>"; 
    }
} // end is page


 if ( is_single()) { 

    
      echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Current Post Title:</span>
        <span class='setting-title pull-right'> " . get_the_title($parent) . "</span>
    </div>";

echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Current Post ID:</span>
        <span class='setting-title pull-right'> " . $currentID . "</span>
    </div>"; 

 $categories = get_the_category();
 $catslug =  $categories[0]->cat_name;
$separator = '<br>';
$output = '';
if ( ! empty( $categories ) ) {
    foreach( $categories as $category ) {
        $output .= '' . esc_html( $category->name ) . ' - ' . $category->slug .'' . $separator;
    }
   
   echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Post Categories and Slugs:</span>
        <span class='setting-title pull-right full-width'> " . trim( $output, $separator ) . "</span>
    </div>";
   
 }  
    
    
    
} // end is post
    

  


    

echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Body Classes:</span>
        <span class='setting-title pull-right full-width'> ";
        foreach ($bodyclasses as $currentBodyClass) {
    echo "$currentBodyClass<br />\n";
}       
        echo "</span>
    </div>"; 
    



    
    // test 
    
    

    
 echo "   
    <!-- end: THEME SWITCHER -->
</div>";
echo "<div class='panel-heading ng-scope'>Query Count & Load Time <i class='fa fa-caret-square-o-down pull-right'></i>
</div>
<div class='panel-body ng-scope'>";

echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Number of queries:</span>
        <span class='setting-title pull-right'> "; echo get_num_queries(); echo "</span>
    </div>"; 

echo "    <div class='setting-box clearfix'>
        <span class='setting-title pull-left ng-scope'>Page load time:</span>
        <span class='setting-title pull-right'> "; timer_stop(1); echo "</span>
    </div>";

echo "
</div>
<!-- end: SETTINGS -->
</div> ";
else : 

endif; 
endif; 

}



add_action( 'wp_head', 'page_info_panel' );

