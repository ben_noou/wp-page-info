# README #

### What does WP-Page-Info Do? ###

WP Page Info allows users/developers to see relevant information from the currently viewed page on the front-end. 

The current features are: 

* Page Title
* Page ID
* Page Parent Title
* Page Parent ID
* Body Classes
* Page Template Name
* Page Template File Name
* Number of queries ran on page
* Query run time
